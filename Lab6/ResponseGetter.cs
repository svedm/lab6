﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using Lab6.Models;

namespace Lab6
{
	public class ResponseGetter<T>
	{
		public IEnumerable<T> Execute(string link)
		{
			try
			{
				using (var webClient = new WebClient())
				{
                    webClient.Encoding = Encoding.UTF8;
					var js = new JavaScriptSerializer ();
					var data = js.Deserialize<ResponseModel<T>> (webClient.DownloadString(link));
					return data.Response.Items;
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Ошибка: {0}", ex.Message);
				return new List<T>();
			}
		}
	}
}

