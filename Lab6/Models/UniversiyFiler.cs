﻿using System.ComponentModel;

namespace Lab6.Models
{
	public class UniversiyFiler
	{
		public UniversiyFiler()
		{
			Q = null;
			CountryId = 1;
			CityId = 1;
			Count = 10;
		}

		[DisplayName("q")]
		public string Q { get; set; }

		[DisplayName("country_id")]
		public int CountryId { get; set; }

		[DisplayName("city_id")]
		public int CityId { get; set; }

		[DisplayName("offset")]
		public int? Offset { get; set; }

		[DisplayName("count")]
		public int Count { get; set; }
	}
}

