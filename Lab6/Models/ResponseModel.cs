﻿using System.Collections.Generic;

namespace Lab6.Models
{
	public class ResponseModel<T>
	{
		public CountItemsModel<T> Response { get; set; }
	}

	public class CountItemsModel<T>
	{
		public int Count { get; set; }

		public IEnumerable<T> Items { get; set; }
	}
}

