﻿using System.ComponentModel;

namespace Lab6.Models
{
	public class CityFilter
	{
		public CityFilter ()
		{
			CountryId = 1;
			Q = null;
			Count = 10;
		}

		[DisplayName("country_id")]
		public int CountryId { get; set; }

		[DisplayName("region_id")]
		public int? RegionId { get; set; }

		[DisplayName("q")]
		public string Q { get; set;}

		[DisplayName("count")]
		public int Count { get; set;}

		[DisplayName("offset")]
		public string Offset { get; set; }
	}
}

