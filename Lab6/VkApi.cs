﻿using System;
using System.Collections.Generic;
using Lab6.Models;

namespace Lab6
{
	public class VkApi
	{
		private readonly string _apiUrl;

		public VkApi(string apiUrl)
		{
			_apiUrl = apiUrl;
		}

		public VkApi()
			:this("https://api.vk.com/method/")
		{
		}

		public IEnumerable<City> GetCities(CityFilter filter)
		{
			return new ResponseGetter<City>().Execute(MakeUlr("database.getCities", filter.ToQueryString()));
		}

		public IEnumerable<University> GetUniversities(UniversiyFiler filter)
		{
			return new ResponseGetter<University>().Execute(MakeUlr("database.getUniversities", filter.ToQueryString()));
		}
			
		private string MakeUlr(string methodName, string queryParams)
		{
			return String.Format("{0}{1}?{2}&v=5.26", _apiUrl, methodName, queryParams);
		}
	}
}

