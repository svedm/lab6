﻿using System;
using System.Threading;
using Lab6.Models;

namespace Lab6
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			Console.WriteLine("Top 10 Городов РФ и Top 10 университетов в каждом");
			var vkapi = new VkApi();
			var cities = vkapi.GetCities(new CityFilter());
			foreach (var city in cities)
			{
				Console.WriteLine("Город: {0}", city.Title);
				Thread.Sleep(300);
				var universities = vkapi.GetUniversities(new UniversiyFiler { CityId = city.Id });
				foreach (var university in universities)
				{
					Console.WriteLine("Институт: {0}", university.Title.Trim());
				}
			}
		    Console.ReadLine();
		}
	}
}
