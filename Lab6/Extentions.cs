﻿using System;
using System.Linq;
using System.ComponentModel;

namespace Lab6
{
	public static class Extentions
	{
		/// <summary>
		/// У каждого поля типа обязательно должен присутствовать атрибут DisplayName
		/// </summary>
		public static string ToQueryString(this object source)
		{
			if (source == null)
				throw new ArgumentNullException("source");

			var properties = source.GetType().GetProperties()
				.Where(x => x.CanRead)
				.Where(x => x.GetValue(source, null) != null)
				.ToDictionary(x => ((DisplayNameAttribute)x.GetCustomAttributes(typeof(DisplayNameAttribute), false).First()).DisplayName, x => x.GetValue(source, null));
				
			return string.Join("&", properties
				.Select(x => string.Concat(
					Uri.EscapeDataString(x.Key), "=",
					Uri.EscapeDataString(x.Value.ToString()))));
		}
	}
}

